package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", getBody).Methods("GET")
	log.Fatal(http.ListenAndServe(":8000", router))
}

func getBody(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte("I love you"))
}